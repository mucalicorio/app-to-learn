import { Component, OnInit } from '@angular/core';
import { ThemesService } from 'src/app/services/themes/themes.service';

const themes = {
  light: {
    primary: '#00A0DC',
    secondary: '#1E4382',
    tertiary: '#58595B',
    dark: '#37373d',
    medium: '#ababab',
    light: '#f4f5f8'
  },
  dark: {
    primary: '#00A0DC',
    secondary: '#1E4382',
    tertiary: '#58595B',
    medium: '#BCC2C7',
    dark: '#bcc2c7',
    light: '#37373d'
  }
};

@Component({
  selector: 'app-themes',
  templateUrl: './themes.page.html',
  styleUrls: ['./themes.page.scss'],
})
export class ThemesPage implements OnInit {

  constructor(private themesService: ThemesService) { }

  ngOnInit() {
  }

  changeTheme(name) {
    console.log('Themes name: ', themes[name]);
    this.themesService.setTheme(themes[name]);
  }
}
