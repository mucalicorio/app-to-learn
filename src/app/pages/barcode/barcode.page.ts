import { Component, OnInit } from '@angular/core';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.page.html',
  styleUrls: ['./barcode.page.scss'],
})
export class BarcodePage implements OnInit {

  barcodeData = { text: '' };

  latLongUser;

  checkValid = { checked: false };

  constructor(
    private barcodeScanner: BarcodeScanner,
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
  }


  callFunctions() {
    if (this.checkValid.checked === true) {
      this.scan();
      this.getLocation();
    } else {
      this.barcodeData.text = 'Cê não me aceitou, nego? :\'(';
      this.latLongUser = { err: 'Cê não me aceitou, nego? :\'(' };
    }
  }

  scan() {
    this.barcodeScanner.scan().then(data => {
      console.log('Barcode Data: ', data);

      this.barcodeData = data;
    }).catch(err => {
      this.barcodeData.text = 'Could not read, may have been a failure in cordova.';
      console.log('Error: ', err);
    });
  }

  getLocation() {
    this.geolocation.getCurrentPosition()
      .then((data) => {
        this.latLongUser = data.coords;
        this.latLongUser.err = null;
        console.log('Barcode Data: ', this.latLongUser);
      })
      .catch((err) => {
        this.latLongUser = { err: 'Cê não me aceitou, nego? :\'(' };
        console.log('Error: ', this.latLongUser.err);
      });
  }

}
