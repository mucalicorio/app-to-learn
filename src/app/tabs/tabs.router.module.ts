import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../pages/home/home.module#HomePageModule'
          }
        ]
      },
      {
        path: 'barcode',
        children: [
          {
            path: '',
            loadChildren: '../pages/barcode/barcode.module#BarcodePageModule'
          }
        ]
      },
      {
        path: 'themes',
        children: [
          {
            path: '',
            loadChildren: '../pages/themes/themes.module#ThemesPageModule'
          }
        ]
      },
      {
        path: 'send',
        children: [
          {
            path: '',
            loadChildren: '../pages/send/send.module#SendPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
